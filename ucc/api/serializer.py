from rest_framework import serializers
from nomina.models import *

# Serializers define the API representation.
class ProgramaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Programa
        fields = ['id', 'cod', 'nombre_pro', 'semestres']


class EmpleadoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Empleado
        fields = ['cedula', 'nombre', 'apellido', 'correo', 'tel', 'tipo_usuario']

