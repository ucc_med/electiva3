from django.urls import path, include
from rest_framework import routers

from .views import *

router = routers.DefaultRouter()
router.register(r'programas', ProgramaViewSet)
router.register(r'empleado', EmpleadoViewSet)

urlpatterns = [
    path('1.0/', include(router.urls)),
    # path('2.0/', include(router.urls)),
]
