from django.shortcuts import render
from nomina.models import *
from rest_framework import viewsets
from .serializer import *
# Create your views here.


# ViewSets define the view behavior.
class ProgramaViewSet(viewsets.ModelViewSet):
    queryset = Programa.objects.all()
    serializer_class = ProgramaSerializer

class EmpleadoViewSet(viewsets.ModelViewSet):
    queryset = Empleado.objects.all()
    serializer_class = EmpleadoSerializer

