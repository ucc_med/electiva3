from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import *
# Create your views here.
from django.contrib import messages
from .crypt import *

def index(request):
    auth = request.session.get("auth", False)
    # control por backend, de autenticación....
    if auth:
        return render(request, "index.html")
    else:
        return redirect("login_form")


def ver_empleados(request):
    # capturar sesión
    auth = request.session.get("auth", False)
    # control de autenticación y autorización
    if auth and (auth["nombre_rol"] == "Administrador" or auth["nombre_rol"] == "Coordinador" ):
        return render(request, "empleados.html")
    else:
        messages.info(request, "Usted no está autorizado para ver este módulo...")
        return redirect("index")


def ver_programas(request):
    return render(request, "programas.html")


def programa_detalle(request, id_programa):
    # capturar sesión
    auth = request.session.get("auth", False)
    # control de autenticación y autorización
    if auth and auth["nombre_rol"] == "Profesor":
        nombre = ""
        if id_programa == 1:
            nombre = "Software"
        elif id_programa == 2:
            nombre = "Mecánica"
        elif id_programa == 3:
            nombre = "Civil"
        else:
            nombre = "Error 404: Programa no existe."

        context = {"programa": nombre}
        return render(request, "programa_detalle.html", context)
    else:
        messages.info(request, "Usted no está autorizado para ver este módulo...")
        return redirect("index")

def formulario1(request):
    return render(request, "formularios/formulario1.html")


def procesar_f1(request):
    num1 = int(request.POST.get("num1"))
    num2 = int(request.POST.get("num2"))
    res = num1 + num2
    return HttpResponse(f"La suma de {num1} + {num2} es <strong>{res}</strong>")


def login_form(request):
    auth = request.session.get("auth", False)
    # control por backend, de autenticación....
    if auth:
        return redirect("index")
    else:
        return render(request, "login/login.html")


def login(request):
    try:
        user = int(request.POST.get("cedula"))
    except:
        messages.warning(request, f"El nombre de Usuario debe ser un documento de identidad.")
        return redirect('login_form')

    passw = request.POST.get("password")
    try:
        consulta = Empleado.objects.get(cedula=user)
        # Usuario válido entonces obtener el hash de la DB.
        if verify_password(passw, consulta.password):
            request.session["auth"] = {
                "nombre": f"{consulta.nombre} {consulta.apellido}",
                "cedula": consulta.cedula,
                "tipo_usuario": consulta.tipo_usuario,
                "nombre_rol": consulta.get_tipo_usuario_display()
            }
            messages.success(request, "Bienvenido!!")
            return redirect('index')
        else:
            # lanzamos excepción para cuando password no concuerda...
            raise Empleado.DoesNotExist()
    except Empleado.DoesNotExist:
        request.session["auth"] = False
        messages.error(request, f"Usuario o contraseña incorrectos...")
        return redirect('login_form')


def logout(request):
    del request.session["auth"]
    return redirect('login_form')


def cambio_passw(request):
    auth = request.session.get("auth", False)
    if auth:
        if request.method == "POST":
            actual = request.POST.get("actual")
            clave1 = request.POST.get("clave1")
            clave2 = request.POST.get("clave2")
            # proceso cambio
            usuario = Empleado.objects.get(pk=auth["cedula"])
            if verify_password(actual, usuario.password):
                if clave1 == clave2:
                    # guardamos la nueva clave encriptada
                    usuario.password = hash_password(clave1)
                    usuario.save()
                    messages.success(request, "Contraseña cambiada correctamente!!")
                    return redirect("index")
                else:
                    messages.warning(request, "Las nuevas claves no concuerdan...")
                    return redirect("cambio_passw")
            else:
                messages.error(request, "La clave actual no coincide...")
                return redirect("cambio_passw")
        else:
            return render(request, "login/cambio_password.html")
    else:
        return redirect("login_form")
def ver_perfil(request):
    pass


def programas(request):
    # Consultar todos los programas
    q = Programa.objects.all()              # Select * from Programa
    # q = Programa.objects.filter(cod=1)      # Select * from Programa WHERE nombre like "% Ingeniería %"
    # q = Programa.objects.get(pk=65)         #  Select * from Programa WHERE id = 65
    contexto = { "data": q }
    return render(request, "programas/listar_programas.html", contexto)

def agregar_programas(request):
    auth = request.session.get("auth", False)
    if auth["nombre_rol"] != "Profesor":
        if request.method == "POST":
            # capturando los datos del formulario a través de los "name's"
            cod = int(request.POST.get("cod"))
            nombre_pro = request.POST.get("nombre_pro")
            semestres = int(request.POST.get("semestres"))
            # Crear un query de inserción en la DB
            q = Programa(
                cod=cod,
                nombre_pro=nombre_pro,
                semestres=semestres
            )
            q.save()
            messages.success(request, "Programa creado correctamente!!")
            return redirect("programas")
        else:
            return render(request, "programas/agregar_programa.html")
    else:
        messages.warning(request, "No está autorizado...")
        return redirect("index")

def eliminar_programa(request, id_programa):
    # Obtener instancia del programa a través del id
    q = Programa.objects.get(pk=id_programa)
    q.delete()
    messages.success(request, "Programa eliminado correctamente!!")
    return redirect("programas")


def actualizar_programa(request, id_programa):
    # Obtener instancia del programa a través del id
    q = Programa.objects.get(pk=id_programa)
    if request.method == "POST":
        # capturando los datos del formulario a través de los "name's"
        cod = int(request.POST.get("cod"))
        nombre_pro = request.POST.get("nombre_pro")
        semestres = int(request.POST.get("semestres"))
        # Crear un query de inserción en la DB
        q.cod=cod
        q.nombre_pro=nombre_pro
        q.semestres=semestres
        q.save()    # update
        messages.success(request, "Programa actualizado correctamente!!")
        return redirect("programas")
    else:
        contexto = { "data": q }
        return render(request, "programas/editar_programa.html", contexto)




