from django.contrib import admin
from .models import Empleado, Programa, EmpleadoPrograma

# Register your models here.
# admin.site.register(Empleado)
# admin.site.register(Programa)
# admin.site.register(EmpleadoPrograma)

@admin.register(Empleado)
class EmpleadoAdmin(admin.ModelAdmin):
    list_display = ['cedula', 'nombre', 'apellido', 'correo', 'tel', 'tipo_usuario', 'password']
    search_fields = ['cedula', 'nombre']
    # list_editable = ['password']

@admin.register(Programa)
class ProgramaAdmin(admin.ModelAdmin):
    list_display = ['id', 'cod', 'nombre_pro', 'semestres']

@admin.register(EmpleadoPrograma)
class EmpleadoProgramaAdmin(admin.ModelAdmin):
    list_display = ['id', 'empleado', 'programa', 'rol']
    list_filter = ['empleado', 'programa']
    list_editable = ['programa', 'rol']