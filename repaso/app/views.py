from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.


def inicio(request):
    import datetime
    fecha = datetime.date.today()
    context = {"datos": fecha}
    return render(request, "index.html", context)
